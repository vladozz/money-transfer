package com.github.vladozz.revolut.utils;

import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.core.Response;

public class HttpStatusUtils {
    public static Response toResponse(HttpStatus.Code httpStatus) {
        return Response.status(httpStatus.getCode()).build();
    }
}
