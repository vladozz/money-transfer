package com.github.vladozz.revolut.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "account")
@NamedQueries({
        @NamedQuery(
                name = Account.FIND_ALL,
                query = "SELECT a FROM Account a"
        )
})
public class Account {
    public static final String FIND_ALL = "com.github.vladozz.revolut.entity.Account.findAll";

    @Id
    @Column(name = "number", nullable = false)
    private String number;

    @Column(name = "balance", nullable = false)
    private BigDecimal balance;

    @JsonIgnore
    @Version
    @Column(name = "version", nullable = false)
    private Long version;
}
