package com.github.vladozz.revolut.entity;

import lombok.Value;

@Value
public class Accounts {
    private final Account debitAccount;
    private final Account creditAccount;
}
