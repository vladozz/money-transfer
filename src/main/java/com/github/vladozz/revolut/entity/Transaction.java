package com.github.vladozz.revolut.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@Entity
@Table(name = "transaction")
@NamedQueries({
        @NamedQuery(
                name = Transaction.FIND_ALL,
                query = "SELECT t FROM Transaction t"
        )
})
public class Transaction {
    public static final String FIND_ALL = "com.github.vladozz.revolut.entity.Transaction.findAll";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private String id;

    @NotBlank
    @Column(name = "debitAccount", nullable = false)
    private String debitAccount;

    @NotBlank
    @Column(name = "creditAccount", nullable = false)
    private String creditAccount;

    @NotNull @DecimalMin(value = "0", inclusive = false)
    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "creationDate", nullable = false)
    private Instant creationDate;

    public Transaction(String debitAccount, String creditAccount, BigDecimal amount) {
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
        this.amount = amount;
    }

    @PrePersist
    public void onCreate() {
        creationDate = Instant.now();
    }
}
