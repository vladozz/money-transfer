package com.github.vladozz.revolut.exception;

public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(String accountNumber) {
        super(String.format("Account '%s' is not found", accountNumber));
    }
}
