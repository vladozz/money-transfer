package com.github.vladozz.revolut.exception;

import java.math.BigDecimal;

public class NotEnoughMoneyException extends RuntimeException {

    public NotEnoughMoneyException(String accountNumber, BigDecimal balance, BigDecimal transferAmount) {
        super(String.format(
                "Account '%s' balance %s not enough to debit %s",
                accountNumber, balance.toPlainString(), transferAmount.toPlainString()));
    }
}
