package com.github.vladozz.revolut;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MoneyTransferConfiguration extends Configuration {

    @NotNull @Min(0)
    private Integer optimisticUpdateAttemptCount;

    @JsonProperty("database")
    @NotNull @Valid
    private DataSourceFactory dataSourceFactory = new DataSourceFactory();
}
