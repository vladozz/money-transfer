package com.github.vladozz.revolut.resources;

import com.github.vladozz.revolut.dao.AccountDAO;
import com.github.vladozz.revolut.entity.Account;
import io.dropwizard.hibernate.UnitOfWork;
import lombok.RequiredArgsConstructor;
import org.hibernate.CacheMode;
import org.hibernate.validator.constraints.NotBlank;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class AccountResource {

    private final AccountDAO accountDAO;

    @GET
    @Path("/{number}")
    @UnitOfWork(readOnly = true, cacheMode = CacheMode.IGNORE)
    public Account getAccount(@PathParam("number") @NotBlank String accountNumber) {
        return accountDAO.findByNumber(accountNumber)
                .orElseThrow(() -> new NotFoundException("Account not found"));
    }
}
