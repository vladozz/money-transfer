package com.github.vladozz.revolut.resources;

import com.github.vladozz.revolut.dao.TransactionDAO;
import com.github.vladozz.revolut.entity.Transaction;
import com.github.vladozz.revolut.exception.AccountNotFoundException;
import com.github.vladozz.revolut.exception.EqualDebitAndCreditAccountException;
import com.github.vladozz.revolut.exception.NotEnoughMoneyException;
import com.github.vladozz.revolut.service.MoneyTransferService;
import io.dropwizard.hibernate.UnitOfWork;
import lombok.RequiredArgsConstructor;
import org.hibernate.CacheMode;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.github.vladozz.revolut.utils.HttpStatusUtils.toResponse;
import static org.eclipse.jetty.http.HttpStatus.Code.UNPROCESSABLE_ENTITY;

@Path("/transactions")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class TransactionResource {

    private final TransactionDAO transactionDAO;
    private final MoneyTransferService moneyTransferService;

    @POST
    public Transaction transfer(@NotNull @Valid Transaction transaction) {
        try {
            return moneyTransferService.transfer(transaction);
        } catch (AccountNotFoundException e) {
            throw new NotFoundException(e.getMessage());
        } catch (EqualDebitAndCreditAccountException e) {
            throw new WebApplicationException("Debit and credit accounts cannot be the same", toResponse(UNPROCESSABLE_ENTITY));
        } catch (NotEnoughMoneyException e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.CONFLICT);
        }
    }

    @GET
    @UnitOfWork(readOnly = true, cacheMode = CacheMode.IGNORE)
    public List<Transaction> getAllTransactions() {
        return transactionDAO.findAll();
    }
}
