package com.github.vladozz.revolut.service;

import com.github.vladozz.revolut.dao.AccountDAO;
import com.github.vladozz.revolut.dao.TransactionDAO;
import com.github.vladozz.revolut.entity.Account;
import com.github.vladozz.revolut.entity.Accounts;
import com.github.vladozz.revolut.entity.Transaction;
import com.github.vladozz.revolut.exception.AccountNotFoundException;
import com.github.vladozz.revolut.exception.EqualDebitAndCreditAccountException;
import com.github.vladozz.revolut.exception.NotEnoughMoneyException;
import io.dropwizard.hibernate.UnitOfWork;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.CacheMode;
import org.hibernate.StaleObjectStateException;

import javax.persistence.OptimisticLockException;
import java.math.BigDecimal;

@Slf4j
@RequiredArgsConstructor
public class MoneyTransferService {

    private final InternalMoneyTransferService internal;
    private final int optimisticLockRetryCount;

    public Transaction transfer(Transaction transaction) {
        if (transaction.getDebitAccount().equals(transaction.getCreditAccount())) {
            throw new EqualDebitAndCreditAccountException();
        }

        for (int i = 0; i < optimisticLockRetryCount; i++) {
            try {
                return internal.transfer(transaction, false);
            } catch (OptimisticLockException e) {
                StaleObjectStateException cause = (StaleObjectStateException) e.getCause();
                if (i < optimisticLockRetryCount - 1) {
                    log.info("Failed attempt to optimistically update the account '{}' due to contention. Try one more time", cause.getIdentifier());
                } else {
                    log.warn("Failed attempt to update the account '{}' due to high contention. {} of optimistic lock attempts limit exceed. Switch to pessimistic lock mode.",
                            cause.getIdentifier(), optimisticLockRetryCount);
                }
            }
        }
        return internal.transfer(transaction, true);
    }

    @RequiredArgsConstructor
    public static class InternalMoneyTransferService {

        private final AccountDAO accountDAO;
        private final TransactionDAO transactionDAO;

        @UnitOfWork(cacheMode = CacheMode.IGNORE)
        public Transaction transfer(Transaction transaction, boolean pessimisticLock) {
            String debitAccountNumber = transaction.getDebitAccount();
            String creditAccountNumber = transaction.getCreditAccount();
            BigDecimal amount = transaction.getAmount();

            Accounts accounts = accountDAO.findByNumbersWithLock(debitAccountNumber, creditAccountNumber, pessimisticLock);
            Account debitAccount = checkIfPresent(accounts.getDebitAccount(), debitAccountNumber);
            Account creditAccount = checkIfPresent(accounts.getCreditAccount(), creditAccountNumber);

            if (debitAccount.getBalance().compareTo(amount) < 0) {
                throw new NotEnoughMoneyException(debitAccount.getNumber(), debitAccount.getBalance(), amount);
            }

            debitAccount.setBalance(debitAccount.getBalance().subtract(amount));
            creditAccount.setBalance(creditAccount.getBalance().add(amount));

            accountDAO.save(debitAccount);
            accountDAO.save(creditAccount);

            transaction.setId(null);
            return transactionDAO.save(transaction);
        }
    }

    private static Account checkIfPresent(Account account, String accountNumber) {
        if (account == null) {
            throw new AccountNotFoundException(accountNumber);
        }
        return account;
    }
}
