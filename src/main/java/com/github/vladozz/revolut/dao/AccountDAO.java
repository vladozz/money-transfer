package com.github.vladozz.revolut.dao;

import com.github.vladozz.revolut.entity.Account;
import com.github.vladozz.revolut.entity.Accounts;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Optional;

import static org.hibernate.LockMode.OPTIMISTIC;
import static org.hibernate.LockMode.PESSIMISTIC_WRITE;

public class AccountDAO extends AbstractDAO<Account> {
    public AccountDAO(SessionFactory factory) {
        super(factory);
    }

    private Account find(String number, boolean pessimisticLock) {
        return currentSession().get(Account.class, number, pessimisticLock ? PESSIMISTIC_WRITE : OPTIMISTIC);
    }

    public Accounts findByNumbersWithLock(String debitAccountNumber, String creditAccountNumber, boolean pessimisticLock) {
        Account debitAccount;
        Account creditAccount;
        if (debitAccountNumber.compareTo(creditAccountNumber) < 0) {
            debitAccount = find(debitAccountNumber, pessimisticLock);
            creditAccount = find(creditAccountNumber, pessimisticLock);
        } else {
            creditAccount = find(creditAccountNumber, pessimisticLock);
            debitAccount = find(debitAccountNumber, pessimisticLock);
        }

        return new Accounts(debitAccount, creditAccount);
    }

    public Account save(Account account) {
        return persist(account);
    }

    public Optional<Account> findByNumber(String number) {
        return Optional.ofNullable(get(number));
    }
}
