package com.github.vladozz.revolut.dao;

import com.github.vladozz.revolut.entity.Transaction;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class TransactionDAO extends AbstractDAO<Transaction> {
    public TransactionDAO(SessionFactory factory) {
        super(factory);
    }

    public Transaction save(Transaction transcation) {
        return persist(transcation);
    }

    public List<Transaction> findAll() {
        return list(namedQuery(Transaction.FIND_ALL));
    }
}
