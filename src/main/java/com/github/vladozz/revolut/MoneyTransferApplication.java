package com.github.vladozz.revolut;

import com.github.vladozz.revolut.dao.AccountDAO;
import com.github.vladozz.revolut.dao.TransactionDAO;
import com.github.vladozz.revolut.entity.Account;
import com.github.vladozz.revolut.entity.Transaction;
import com.github.vladozz.revolut.resources.AccountResource;
import com.github.vladozz.revolut.resources.TransactionResource;
import com.github.vladozz.revolut.service.MoneyTransferService;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWorkAwareProxyFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfiguration> {
    public static void main(String[] args) throws Exception {
        new MoneyTransferApplication().run(args);
    }

    private final HibernateBundle<MoneyTransferConfiguration> hibernateBundle =
            new HibernateBundle<MoneyTransferConfiguration>(Account.class, Transaction.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(MoneyTransferConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    @Override
    public String getName() {
        return "revolut-money-transfer";
    }

    @Override
    public void initialize(Bootstrap<MoneyTransferConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );

        bootstrap.addBundle(new MigrationsBundle<MoneyTransferConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(MoneyTransferConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
    }

    @Override
    public void run(MoneyTransferConfiguration configuration, Environment environment) {
        final AccountDAO accountDAO = new AccountDAO(hibernateBundle.getSessionFactory());
        final TransactionDAO transactionDAO = new TransactionDAO(hibernateBundle.getSessionFactory());

        final MoneyTransferService.InternalMoneyTransferService internalTransferService =
                new UnitOfWorkAwareProxyFactory(hibernateBundle)
                        .create(
                                MoneyTransferService.InternalMoneyTransferService.class,
                                new Class[]{
                                        AccountDAO.class,
                                        TransactionDAO.class
                                },
                                new Object[]{
                                        accountDAO,
                                        transactionDAO
                                });

        final MoneyTransferService transferService = new MoneyTransferService(internalTransferService, configuration.getOptimisticUpdateAttemptCount());

        environment.jersey().register(new AccountResource(accountDAO));
        environment.jersey().register(new TransactionResource(transactionDAO, transferService));
    }
}
