package com.github.vladozz.revolut;

import com.github.vladozz.revolut.entity.Account;
import com.github.vladozz.revolut.entity.Transaction;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import lombok.SneakyThrows;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.shuffle;

public class IntegrationTest {

    private static final String ACCOUNT_1 = "10000001";
    private static final String ACCOUNT_2 = "10000002";
    private static final String NON_EXISTING_ACCOUNT = "111";

    private static final int ACCOUNT_1_INITIAL_BALANCE = 10_000;
    private static final int ACCOUNT_2_INITIAL_BALANCE = 20_000;

    private final String tempDbFile = createTempFile();
    private final String configPath = ResourceHelpers.resourceFilePath("test-money-transfer.yml");

    private String contextRoot;

    @Rule
    public final DropwizardAppRule<MoneyTransferConfiguration> RULE = new DropwizardAppRule<>(
            MoneyTransferApplication.class, configPath,
            ConfigOverride.config("database.url", "jdbc:h2:" + tempDbFile + ";MVCC=TRUE;LOCK_TIMEOUT=10000"));

    @Before
    public void migrateDb() throws Exception {
        RULE.getApplication().run("db", "migrate", configPath);

        contextRoot = UriBuilder.fromUri("http://localhost").port(RULE.getLocalPort()).toString();
    }

    @SneakyThrows
    private static String createTempFile() {
        return File.createTempFile("test-money-transfer", null).getAbsolutePath();
    }

    @Test
    public void successfulTranfer() {
        Transaction transaction = requestTransfer(ACCOUNT_1, ACCOUNT_2, 5).readEntity(Transaction.class);
        Assert.assertNotNull("transaction id", transaction.getId());

        assertAccountBalance(ACCOUNT_1, ACCOUNT_1_INITIAL_BALANCE - 5);
        assertAccountBalance(ACCOUNT_2, ACCOUNT_2_INITIAL_BALANCE + 5);

        assertTransactions(1);
    }

    private Response requestTransfer(String debitAccount, String creditAccount, Integer amount) {
        Transaction transaction = new Transaction(
                debitAccount,
                creditAccount,
                amount == null ? null : BigDecimal.valueOf(amount));

        return RULE.client()
                .target(contextRoot).path("transactions")
                .request()
                .post(Entity.json(transaction));
    }

    private void assertAccountBalance(String accountNumber, double balance) {
        Account account =
                RULE.client()
                        .target(contextRoot).path("accounts").path(accountNumber)
                        .request()
                        .get()
                        .readEntity(Account.class);

        Assert.assertEquals("Balance of account " + accountNumber, new BigDecimal(balance), account.getBalance());
    }

    private void assertTransactions(int count) {
        List<Transaction> transactions =
                RULE.client()
                        .target(contextRoot).path("transactions")
                        .request()
                        .get()
                        .readEntity(new GenericType<List<Transaction>>() {});

        Assert.assertEquals("Transactions count ", count, transactions.size());
    }

    @Test
    public void notEnoughMoney() {
        Response response = requestTransfer(ACCOUNT_1, ACCOUNT_2, 50_000);

        Assert.assertEquals("response http status", HttpStatus.CONFLICT_409, response.getStatus());
    }

    @Test
    public void debitAccountNotFound() {
        Response response = requestTransfer(NON_EXISTING_ACCOUNT, ACCOUNT_1, 5);

        Assert.assertEquals("response http status", HttpStatus.NOT_FOUND_404, response.getStatus());
    }

    @Test
    public void creditAccountNotFound() {
        Response response = requestTransfer(ACCOUNT_1, NON_EXISTING_ACCOUNT, 5);

        Assert.assertEquals("response http status", HttpStatus.NOT_FOUND_404, response.getStatus());
    }

    @Test
    public void debitAccountIsMissing() {
        Response response = requestTransfer(null, ACCOUNT_2, 5);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    public void creditAccountIsMissing() {
        Response response = requestTransfer(ACCOUNT_1, null, 5);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    public void debitAndCreditAccountsAreTheSame() {
        Response response = requestTransfer(ACCOUNT_1, ACCOUNT_1, 5);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    public void amountIsMissing() {
        Response response = requestTransfer(ACCOUNT_1, ACCOUNT_1, null);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    public void amountIsZero() {
        Response response = requestTransfer(ACCOUNT_1, ACCOUNT_2, 0);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    @Test
    public void amountIsNegative() {
        Response response = requestTransfer(ACCOUNT_1, ACCOUNT_2, -5);

        Assert.assertEquals("response http status", HttpStatus.UNPROCESSABLE_ENTITY_422, response.getStatus());
    }

    /**
     * Number of requests is 3*2 times more than config property 'optimisticUpdateAttemptCount'. Most updates are switched to pessimistic.
     */
    @Test
    public void parallelUpdatesPessimistic() throws Exception {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            threads.add(new Thread(() -> requestTransfer(ACCOUNT_1, ACCOUNT_2, 5)));
            threads.add(new Thread(() -> requestTransfer(ACCOUNT_2, ACCOUNT_1, 10)));
        }
        shuffle(threads);  //add a bit entropy
        startAndWaitForAll(threads);

        assertAccountBalance(ACCOUNT_1, ACCOUNT_1_INITIAL_BALANCE + (10 - 5) * 30);
        assertAccountBalance(ACCOUNT_2, ACCOUNT_2_INITIAL_BALANCE + (5 - 10) * 30);

        assertTransactions(30 * 2);
    }

    private void startAndWaitForAll(List<Thread> threads) throws InterruptedException {
        threads.forEach(Thread::start);
        for (Thread thread : threads) {
            thread.join();
        }
    }

    /**
     * Number of requests is equal to config property 'optimisticUpdateAttemptCount'. All updates are optimistic.
     */
    @Test
    public void parallelUpdatesOptimistic() throws Exception {
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            threads.add(new Thread(() -> requestTransfer(ACCOUNT_1, ACCOUNT_2, 5)));
        }
        startAndWaitForAll(threads);

        assertAccountBalance(ACCOUNT_1, ACCOUNT_1_INITIAL_BALANCE - 5 * 10);
        assertAccountBalance(ACCOUNT_2, ACCOUNT_2_INITIAL_BALANCE + 5 * 10);

        assertTransactions(10);
    }
}
