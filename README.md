## Running The Application

* Run following command to build app:  

    `mvn clean package`

* For H2 db migration the following command:  

    `java -jar ./target/money-transfer-1.0.0.jar db migrate money-transfer.yml`

* To run the server run the following command:  

    `java -jar target/money-transfer-1.0.0.jar server money-transfer.yml`

* New transaction sample request:

    ```
    POST http://localhost:8080/transactions
    Content-Type: application/json
    {
    "debitAccount": "10000001",
    "creditAccount": "10000002",
    "amount": 100
    }
    ```

* Get account balance sample request:  

    `GET http://localhost:8080/accounts/10000001`
